<?php
require_once('core.php');

if (isset($_POST['email'])) {
    $data = [
        'nama' => @$_POST['nama'],
        'email' => @$_POST['email'],
        'tgllahir' => @$_POST['tgllahir'],
        'prodi1' => @$_POST['prodi1'],
        'prodi2' => @$_POST['prodi2']
    ];

    list($status,$msg) = Core::httpPost("http://192.168.1.7:9999/api/pendaftar/create",$data);

    if($status == 201){
        header("location:../dashboard.php");
    }else{
        echo "<script>
            alert('".$msg."');
            window.location = '../form.php'
        </script>" ;
    }
}
