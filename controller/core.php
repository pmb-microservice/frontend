<?php

class Core
{
    public static function httpPost($url,$data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_data = curl_exec($curl);
        curl_close($curl);
        $response_data = json_decode($curl_data,true);
       
        return array($response_data['status'],$response_data['message']);
    }

    public static function httpGet($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_data = curl_exec($curl);
        curl_close($curl);
        $response_data = json_decode($curl_data,true);
       
        return array($response_data['status'],$response_data['data']);
    }
}
