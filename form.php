<?php
  require_once('controller/core.php');
  
  //untuk mendapatkan prodi
  list($status,$listprodi) = Core::httpGet("http://192.168.1.7:9999/api/prodi");
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="assets/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet">
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">

    <title>SPMB</title>
  </head>
  <body>
    <section id="page-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-12 mx-auto">
            <div class="card border-0 shadow rounded-3 my-5">
              <div class="card-body p-4 p-sm-5">
                <form action="controller/form.php" method="POST" id='formpendaftar'>
                <div class="mb-3">
                    <label for="labelnama" class="form-label">Nama</label>
                    <input name="nama" type="text" class="form-control" id="labelnama">
                </div>
                <div class="mb-3">
                    <label for="labelemail" class="form-label">Email</label>
                    <input name="email" type="email" class="form-control" id="labelemail">
                </div>
                <div class="mb-3">
                    <label for="labeltgllahir" class="form-label">Tgl Lahir</label>
                    <input name="tgllahir" type="text" class="form-control" id="labeltgllahir">
                </div>
                <div class="mb-3">
                    <label for="prodi2" class="form-label">Pilihan Prodi 1</label>
                    <select class="form-control" name="prodi1"">
                      <?php foreach ($listprodi as $key => $value) { ?>
                        <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="prodi2" class="form-label">Pilihan Prodi 2</label>
                    <select class="form-control" name="prodi2"">
                      <?php foreach ($listprodi as $key => $value) { ?>
                        <option value="<?= $value['id'] ?>"><?= $value['nama'] ?></option>
                      <?php } ?>
                    </select>
                </div>
                <button type="submit" name='btnsubmit' value="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script src="assets/bootstrap/js/bootstrap.bundle.js" ></script>
    <script src="assets/bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  </body>
</html>